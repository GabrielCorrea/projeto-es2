# Projeto Final de Engenharia de Software 2
## Descrição:
### Deve haver um documento com orientações do processo a ser seguido para utilização das ferramentas e dos documentos propostos.

#### Elaborar os artefatos de PMO, contendo ferramentas e documentos usados para o gerenciamento do projeto:

1. Termo de Abertura de Projeto
    -Plano de Gerenciamento de Projetos
    - Definir as Entregas
    - Gerenciar solicitações de mudanças

2. Gerenciar Escopo
    - Requisitos
    - Escopo
    - EAP
    - Verificação e Controle do Escopo
    - *Deve haver Linha de Base 

3. Gerenciar Tempo
    - Definir, sequenciar e estimar atividades
    - Desenvolver um cronograma
    - Controlar Cronograma
    - *Deve haver Linha de Base

4. Gerenciar Qualidade
    - Definir Métricas de Qualidade
    - Definir Lista de Verificação de Qualidade
    - Realizar Garantia de Qualidade
    - Realizar Controle de Qualidade
    - Deve haver Linha de Base

Periodicamente o processo proposto será simulado (a partir das orientações dadas) com o uso das ferramentas e preenchimento dos documentos. 


O processo será simulado como projeto alterações no JabRef:

1. Implementar a busca de arquivo .PDF (e, opcionalmente, .PS, .ODT, .DOCX, .DOC,  .PPT, .PPTX, .ODP) conforme o título do item de bibliografia em consulta.
1.1. Considerar que uma entrada bibtex pode ter crossref.
2. Mostrar a quantidade de citações do documento.
3. Mostrar o fator de impacto do local em que o documento foi publicado.
